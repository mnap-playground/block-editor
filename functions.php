<?php
/**
 * Block Editor functions and definitions.
 * Block Editor is the Seedlet child theme.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Block Editor
 * @since 1.0.0
 */

/**
 * Seedlet only works in WordPress 4.7 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.7', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

/**
 * Block patterns.
 */
require get_stylesheet_directory() . '/inc/block-patterns.php';

/**
 * Block styles.
 */
require get_stylesheet_directory() . '/inc/block-styles.php';
