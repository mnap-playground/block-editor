<?php
/**
 * Block Editor Child Theme: Block Patterns
 *
 * @package Block Editor
 * @since   1.0.0
 */

/**
 * Register Block Pattern Category.
 */
if ( function_exists( 'register_block_pattern_category' ) ) {

	register_block_pattern_category( // phpcs:ignore WPThemeReview.PluginTerritory.ForbiddenFunctions.editor_blocks_register_block_pattern_category
		'blked',
		array( 'label' => __( 'Block Editor', 'blked' ) )
	);
}

/**
 * Register Block Patterns.
 */
if ( function_exists( 'register_block_pattern' ) ) {
	register_block_pattern( // phpcs:ignore WPThemeReview.PluginTerritory.ForbiddenFunctions.editor_blocks_register_block_pattern
		'blked/green-hero',
		array(
			'title'         => __( 'Green Hero', 'blked' ),
			'categories'    => array( 'blked' ),
			'viewportWidth' => 1280,
			'content'       => "<!-- wp:cover {\"url\":\"http://block-editor.test/wp-content/uploads/2021/03/1045-3936x2624-1-scaled.jpg\",\"id\":14,\"dimRatio\":0,\"minHeight\":100,\"minHeightUnit\":\"vh\",\"contentPosition\":\"center left\",\"align\":\"full\"} -->\n<div class=\"wp-block-cover alignfull has-custom-content-position is-position-center-left\" style=\"min-height:100vh\"><img class=\"wp-block-cover__image-background wp-image-14\" alt=\"\" src=\"http://block-editor.test/wp-content/uploads/2021/03/1045-3936x2624-1-scaled.jpg\" data-object-fit=\"cover\"/><div class=\"wp-block-cover__inner-container\"><!-- wp:group {\"backgroundColor\":\"secondary\"} -->\n<div class=\"wp-block-group has-secondary-background-color has-background\"><div class=\"wp-block-group__inner-container\"><!-- wp:heading {\"level\":1,\"style\":{\"typography\":{\"lineHeight\":\"1.5\"}}} -->\n<h1 style=\"line-height:1.5\">Block Patterns</h1>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"fontSize\":\"large\"} -->\n<p class=\"has-large-font-size\">Block patterns example with greenish background</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Vivamus iaculis, dui dolor, rhoncus eget, ultricies accumsan. In euismod nonummy. Sed nonummy auctor vulputate luctus. Aenean libero. Integer tristique velit, condimentum est eu tempor enim dictum velit, vitae ornare interdum, lacus.</p>\n<!-- /wp:paragraph --></div></div>\n<!-- /wp:group --></div></div>\n<!-- /wp:cover -->",
		)
	);
}
