<?php
/**
 * Block Editor Child Theme: Block Styles
 *
 * @package Block Editor
 * @since 1.0.0
 */

if ( function_exists( 'register_block_style' ) ) {
	/**
	 * Register block styles.
	 */
	function blked_register_block_styles() {
		register_block_style( // phpcs:ignore WPThemeReview.PluginTerritory.ForbiddenFunctions.editor_blocks_register_block_style
			'core/list',
			array(
				'name'         => 'blked-square-list',
				'label'        => __( 'Square list', 'blked' ),
				'inline_style' => '.wp-block.is-style-blked-square-list, ul.is-style-blked-square-list { list-style-type: square; }',
			)
		);
	}
	add_action( 'init', 'blked_register_block_styles' );
}
